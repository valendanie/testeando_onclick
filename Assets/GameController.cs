using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class GameController : MonoBehaviour
{
    [SerializeField]
    private GameObject miCosito;
    [SerializeField]
    private Tilemap miTilemap;
    private GameObject Torreta;
    bool construyendo=false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (construyendo)
        {
            Vector3Int position = miTilemap.WorldToCell(Camera.main.ScreenToWorldPoint(Input.mousePosition));
            Torreta.transform.position = new Vector3(position.x,position.y,-3);
            if (Input.GetMouseButtonDown(0))
            {
                construyendo = false;
            }
        }
    }
    public void CrearCosito()
    {
        if (construyendo == false)
        {
        print("hola estoy creando un cosito");
        Torreta = Instantiate(miCosito);
        construyendo = true;
        }
    }
}
